.global _start
.text
_start:
    # Vamos colocar o valor 7 no rdi
    mov $7, %rdi
    # executo 
    syscall

    # limpando o valor do registrador rdi
    xor %rdi, %rdi

    # saindo...
    mov $60, %rax
    syscall
