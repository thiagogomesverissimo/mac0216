#   Compilando:
#     $ as main.s -o main.o
#     $ ld -o main.bin main.o
# Ou de forma mais rápida:
#     $ as main.s -o main.o && ld -o main.bin main.o
#     $ ./main.bin

.global _start
.text
_start:
    # fluxo: 
    # 1. definimos algumas instruções inserindo ou modificando valores nos registradores
    # 2. chamamos o kernel com syscall para execurtar a ação

    # Neste caso vou passar 60 no rax, que significa exit
    mov $60, %rax
    syscall
# Assim, esse programa não faz nada, apenas começa e termina, mas servirá de esqueleto
# para os demais
