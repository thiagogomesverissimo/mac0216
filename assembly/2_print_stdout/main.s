.global _start
.text

message: .ascii "ola\n"

_start:
    # 1 no registrador %rax significa que queremos escrever na saída padrão
    mov $1, %rax
    # %rsi recebe o conteúdo que deverá ser enviado a saída padrão
    mov $message, %rsi
    # %rdx recebe o tamanho que deverá ser usado na saída padrão, no caso 4 caracters
    mov $4, %rdx
    # executo
    syscall

    # saindo...
    mov $60, %rax
    syscall
