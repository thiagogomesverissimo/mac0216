Use o *as* como se fosse um compilador normal, mas não se esqueça de fazer a ligação depois:

    as -o prog.o prog.s
    ld -o prog prog.o

Para gerar uma listagem, use o seguinte exemplo:

    as -alh prog.s > prog.lst


Atenção:

Para montar programas de 32bits no Linux de 64, é preciso colocar opções adicionais tanto no as como no ld :

    as  -prog.o --32 prog.s
    ld -melf_i386 -o prog prog.o

Dependendo da versão do as e do ld, pode ser que esses 
programas reconheçam automaticamente que se trata de 32 bits.
