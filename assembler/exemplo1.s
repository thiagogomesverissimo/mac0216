/* Esse é meu primeiro exemplo com assembly 
 * Para compilar:
 * as -o hello.o hello.s
 * ld -o hello.bin hello.o
 * Ou em um comando apenas: as -o hello.o hello.s && ld -o hello.bin hello.o
 */
    .global _start
    .text 
_start: 
    /* Na primeira instrução rax recebe o número do serviço desejado */

/*
    mov $1, %rax       
    mov $1, %rdi       
    mov $message, %rsi 
    mov $tam, %rdx     
    syscall     

    mov $60, %rax      
    xor %rdi, %rdi     
*/
    syscall      

    message: .ascii "Hello, world!\n"
    tam = . - message
