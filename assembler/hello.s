/* Esse é meu primeiro exemplo com assembly 
 * Para compilar:
 * as -o hello.o hello.s
 * ld -o hello.bin hello.o
 * Ou em um comando apenas: as -o hello.o hello.s && ld -o hello.bin hello.o
 * rodar: ./hello.bin
 */
    .global _start /* coloca na tabela de simbolos que _start é um simbolo especial, como se fo */
    .text /* Segmento de texto */

_start: 
    /* Na primeira instrução rax recebe o número do serviço desejado */
    /* $ is used to refer to the current address */
    mov $1, %rax       /* coloca 1 no rax - significa saida padrão  */
    mov $1, %rdi       /* */
    mov $message, %rsi /* Passar o endereço de message para rsi, valor do label*/
    mov $tam, %rdx     
    syscall     

    mov $60, %rax     /* Código de saída */ 
    xor %rdi, %rdi     
    syscall      

message: .ascii "Hello, world!\n"
tam = . - message /*Calcula o tamanho da string que será printada*/


